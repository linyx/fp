(ns todo.db.db
  (:use carica.core
    korma.core
    korma.db))

(defdb db {:classname   (config :db :classname)
           :subprotocol (config :db :subprotocol)
           :user        (config :db :user)
           :password    (config :db :password)
           :subname     (config :db :subname)
           :delimiters  (config :db :delimiters)})

(declare user lists)
(declare task lists)
(declare task_has_user lists)

(defentity user
  (belongs-to task_has_user))
(defentity task
  (belongs-to task_has_user))
(defentity task_has_user
   (has-one user)
   (has-one task))