(ns todo.controllers.account
  (:require [ring.util.response :as response]
            [clj-http.client :as client]
            [todo.layout :as layout]
            [todo.layout :refer [error-page]]
            [todo.oauth :as oauth]
            [todo.util.utils :as utils]
            [todo.model.user :as user]
            [todo.model.task :as task]
            [clj-time.core :as t]
            [clj-time.coerce :as coerce]))

(defn login-vk[_]
  (response/redirect (oauth/url-auth (utils/generate-state))))

(defn complete[request]
  (let [{params :params session :session} request]
    (println params)
    (if-not (contains? params :error)
      (if (= (params :state) (params :redirect_state))
        (do
          (println
            (oauth/url-access-token (params :code) (params :redirect_state)))
          (let [request_token
                (utils/convert-to-edn
                  (:body
                    (client/get
                      (oauth/url-access-token (params :code) (params :redirect_state)))))]
            (println (oauth/url-get-profile (:access_token request_token)))
            (let [user_info
                  (first(:response (utils/convert-to-edn
                    (:body
                      (client/get
                        (oauth/url-get-profile (:access_token request_token)))))))]
              (println (first (:response user_info)))

            (println request_token)
            (let [user_id (:user_id request_token) last (coerce/to-sql-time(t/now))]
              (let [session
                    (assoc {} :user_info (.get-user-name (if (empty? (user/get-by-vkid user_id))
                      (user/create-user (:user_id request_token) (:first_name user_info) (:last_name user_info) last)
                      (user/update-user (merge (user/get-by-vkid user_id) user_info) last))) :user_id (:user_id request_token))]

                (println session)
                (utils/increment-counter)
                (utils/log session "log")
                (-> (response/redirect "/")
                    (assoc :session session))))
            ))))
      (response/redirect "/")
      )))

(defn authorize?[request]
  (let [session (:session request)]
    (contains? session :user_id)))

(defn logout[request]
  (let [session (:session request)]
    (-> (response/redirect "/")
        (assoc :session (dissoc session :user_id)))))

(defn account-page [request]
  (println (:session request))
  (layout/render "account.html" {:name (:user_info (:session request))
                                 :tasks
                                       (task/get-task-by-user-id
                                         (user/get-id-user-by-vkid (:user_id (:session request))))}))

;dsl

(defn show-page-or-not-found
  [request auth-page auth-area?]
  (if auth-area?
    (if (authorize? request)
      (auth-page request)
      (error-page {:status 404
                   :title "page not found"}))
    (if (authorize? request)
      (error-page {:status 404
                   :title "page not found"})
      (auth-page request)))
  )

(defn show-page
  ([request auth-page]
   (if (authorize? request)
     (auth-page request)
     (error-page {:status 404
                  :title "page not found"})))
  ([request auth unauth]
   (if (and (string? unauth) (not (string? auth)))
     (if (authorize? request)
       (auth request)
       (response/redirect unauth))
     (if (and (string? auth) (not (string? unauth)))
       (if (authorize? request)
         (response/redirect auth)
         (unauth request))))
    ))
