(ns todo.model.task
  (:use korma.core
        korma.db)
  (:require
    [todo.db.db :as db]))

(defprotocol task-protocol
  (get-parent-task [this])
  )

(defrecord task [id title desc is-completed parent_id datetime]
  task-protocol
  (get-parent-task[this]
    (if-not (nil? parent_id)
      (map->task(first (select db/task (where {:id parent_id})))))))

(defn create-task[title desc is_completed parent_id datetime user_id]
  (println title)
  (println desc)
  (try
    (transaction
      (let [id (:generated_key (insert db/task (values{:title  title
                            :desc   desc
                            :is_completed is_completed
                            :parent_id parent_id
                            :datetime datetime})))]
          (task. id title desc is_completed parent_id datetime)
          (insert db/task_has_user (values {:task_id id
                                           :user_id user_id}))))
    (catch Exception e
      (println e)
      (throw (Exception. "error insert task")))))

(defn get-task-by-id [id]
  (map->task(first (select db/task (where {:id id})))))

(defn get-task-by-user-id [user_id]
  (map #(map->task %)
       (select db/task_has_user
        (fields "task.*")
        (join db/task (= :task.id :task_id))
        (where {:user_id user_id})
                                  )))

(defn delete-task[id]
  (try
    (transaction
      (delete db/task_has_user (where {:task_id id}))
      (delete db/task (where {:id id})))
    (catch Exception e
      (println e)
      (throw (Exception. "error delete task"))))
  )

(defn update-task[task]
  (try
      (update db/task
              (set-fields (dissoc (into {} task) :id))
              (where {:id (:id task)}))
      (catch Exception e
        (println e)
        (throw (Exception. "error update task"))))
  )

(defn mark-completed-task[id is_completed]
  (update-task (merge (get-task-by-id id) {:is_completed is_completed})))