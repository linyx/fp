(ns todo.model.user
  (:use korma.core
        korma.db)
  (:require
    [todo.db.db :as db]))

(defprotocol user-protocol
  (get-user-name [this]))

(defrecord user [vkid first_name last_name]
  user-protocol
  (get-user-name [this]
    (str first_name " " last_name)))

(defn get-by-vkid [vkid]
  (let [user (select db/user (where {:vkid vkid}))]
    (if (empty? user)
      '()
      (map->user(dissoc (first user) :last)))
    ))

(defn get-id-user-by-vkid[vkid]
  (:id (first (select db/user (where {:vkid vkid})))))

(defn create-user [vkid first_name last_name last]
  (try
    (println "inserted user")
    (insert db/user (values{:vkid   vkid
                         :first_name  first_name
                         :last_name   last_name
                         :last last}))
    (user. vkid first_name last_name)
    (catch Exception e
      (println e)
      (throw (Exception. "Vk id fail")))))

(defn update-user[user last]
  (try
    (println "update user")
    (println (:first_name user))
      (update db/user
              (set-fields {:first_name (:first_name user) :last_name (:last_name user) :last last})
              (where {:vkid (:vkid user)}))
    user
    (catch Exception e
      (println e)
      (throw (Exception. "error update user")))))


(defn delete-unactive-user-by-date[date]
  (try
    (transaction
      (let [ids (map (fn [item] (let [id (:id item)]
                                  (delete db/task_has_user (where {:user_id id}))
                                  (delete db/user (where {:id id}))
                                  id))
      (select db/user (where {:last [< date]})))] (println ids)))
    (catch Exception e
      (println e)
      (throw (Exception. "error delete users")))))