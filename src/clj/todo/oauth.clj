(ns todo.oauth)

;dsl

(def client-id "5724983")
(def client-secret "nRoQB0GtbxdmPHQc1bG0")

(def auth-params {
                  :client-id client-id
                  :client-secret client-secret
                  :redirect-url "http://localhost:3000/login/complete"
                  :authorize-url "https://oauth.vk.com/authorize?"
                  :access-token-url "https://oauth.vk.com/access_token?"})

(defn url-auth [csrf-token]
  (str (auth-params :authorize-url)
       "client_id="
       (auth-params :client-id)
       "&display=page&redirect_uri="
       (auth-params :redirect-url)
       "?redirect_state="
       csrf-token
       "&response_type=code&v=5.60"
       "&state="
       csrf-token))

(defn url-access-token[code csrf-token]
  (str (auth-params :access-token-url)
       "client_id="
       (auth-params :client-id)
       "&client_secret="
       (auth-params :client-secret)
       "&redirect_uri="
       (auth-params :redirect-url)
       "?redirect_state="
       csrf-token
       "&code="
       code))

(defn url-get-profile[token]
     (str
          "https://api.vk.com/method/users.get?access_token="
          token
          "&v=5.60"
          "&client_secret="
          (auth-params :client-secret)))

(defn create-url[token method & params]
     (str
          "https://api.vk.com/method/"
          method
          "?access_token="
          token
          (apply str
                 (map
                      (fn [item] (str "&" (name (first item)) "=" (second item)))
                      (seq (first params))))
          "&v=5.60"
          "&client_secret="
          (auth-params :client-secret)
          ))

#_(println (create-url "token" "user.get" {:a "rrr" :b "665"}))