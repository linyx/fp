(ns todo.routes.home
  (:require [todo.layout :as layout]
            [compojure.core :refer [defroutes GET POST]]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [ring.util.response :as response]
            [clojure.java.io :as io]
            [todo.db.db :as db]
            [todo.layout :refer [error-page]]
            [todo.controllers.account :as ac]
            [todo.model.user :as user]
            [todo.model.task :as task]
            [clj-time.core :as t]
            [clj-time.coerce :as coerce]
            [todo.validation.task :as v]
            [clj-time.coerce :as c])
 )

(defn home-page [request]
  (println (:session request))
  (layout/render "home.html"))

(defn edit-page [request]
  (layout/render "edit.html" {:name (:user_info (:session request))
                             }))

(defn newtask[request]
  (task/create-task (str "title" (:user_id (:session request))) "desc" 0 nil (coerce/to-sql-time(t/date-time 2016 12 21 0 12 0))
    (user/get-id-user-by-vkid
      (:user_id (:session request))))
  (response/redirect "/"))

(defroutes home-routes
           (GET "/" request
             (ac/show-page request "/account" home-page))
           (GET "/account" request
              (ac/show-page request ac/account-page "/"))
           (GET "/login/vk" request
             (ac/show-page-or-not-found request ac/login-vk false))
           (GET "/login/complete" request
             (ac/complete request))
           (GET "/account/logout" request
             (ac/show-page-or-not-found request ac/logout true))
           (GET "/newtask" request
             (ac/show-page-or-not-found request newtask true))
           (GET "/add" request
             (ac/show-page-or-not-found request edit-page true))
           (POST "/add" request
             (if (nil? (v/validate-task (task/->task nil ((:form-params request) "title") ((:form-params request) "desc") nil nil nil)))
               (do
                 (task/create-task ((:form-params request) "title") ((:form-params request) "desc") 0 nil (c/to-sql-date (t/now)) (user/get-id-user-by-vkid (:user_id (:session request))))
                 (response/redirect "/"))
               (response/redirect "/add")
               ))
           (GET "/delete/:id" [id]
             (task/delete-task id)
             (response/redirect "/"))
           )


