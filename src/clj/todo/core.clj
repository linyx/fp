(ns todo.core
  (:require [todo.handler :as handler]
            [luminus.repl-server :as repl]
            [luminus.http-server :as http]
            [todo.config :refer [env]]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.tools.logging :as log]
            [mount.core :as mount]
            [clojurewerkz.quartzite.scheduler :as qs]
            [clojurewerkz.quartzite.triggers :as t]
            [clojurewerkz.quartzite.jobs :as j]
            [clojurewerkz.quartzite.jobs :refer [defjob]]
            [clojurewerkz.quartzite.schedule.simple :refer [schedule with-interval-in-days
                                                            with-interval-in-minutes repeat-forever]]
            [clj-time.core :as time]
            [clj-time.coerce :as c]
            [todo.model.user :as u])
  (:gen-class))

(def cli-options
  [["-p" "--port PORT" "Port number"
    :parse-fn #(Integer/parseInt %)]])

(mount/defstate ^{:on-reload :noop}
                http-server
                :start
                (http/start
                  (-> env
                      (assoc :handler (handler/app))
                      (update :port #(or (-> env :options :port) %))))
                :stop
                (http/stop http-server))

(mount/defstate ^{:on-reload :noop}
                repl-server
                :start
                (when-let [nrepl-port (env :nrepl-port)]
                  (repl/start {:port nrepl-port}))
                :stop
                (when repl-server
                  (repl/stop repl-server)))


(defn stop-app []
  (doseq [component (:stopped (mount/stop))]
    (log/info component "stopped"))
  (shutdown-agents))

(defn start-app [args]
  (doseq [component (-> args
                        (parse-opts cli-options)
                        mount/start-with-args
                        :started)]
    (log/info component "started"))
  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-app)))

(def ^:private number-unactive-days 100)

(defjob delete-unactive-job [ctx]
        (println "Start delete not active user")
        (u/delete-unactive-user-by-date (c/to-sql-time (time/minus (time/now) (time/days number-unactive-days)))))


(defn -main [& args]
  (let [s   (-> (qs/initialize) qs/start)
        job (j/build
              (j/of-type delete-unactive-job)
              (j/with-identity (j/key "jobs.noop.1")))
        trigger (t/build
                  (t/with-identity (t/key "triggers.1"))
                  (t/start-now)
                  (t/with-schedule (schedule
                                     (with-interval-in-days 10)
                                     (repeat-forever)
                                     )))]
    (qs/schedule s job trigger))
  (start-app args))
