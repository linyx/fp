(ns todo.validation.task
  (:require [bouncer.core :as b]
      [bouncer.validators :as v]))

(defn validate-task [params]
  (first
    (b/validate
      params
      :title v/required
      )))

