(ns todo.util.utils
  (:require [crypto.random :as random]
            [clojure.string :as str]
            [clojure.data.json :as json]
            [clj-time.core :as t]
            [clj-time.coerce :as c]
            [todo.model.user :as u]
            )
  (:import (java.io FileWriter)))

(defn generate-state
  []
  (str/replace (random/base64 60) #"[\+=/]" "-"))

(defn convert-to-edn[data]
  (json/read-str data :key-fn keyword ))

(def login-via-vk-counter (atom 0))

(defn reset-counter[]
  (reset! login-via-vk-counter 0))


(defn log-file-name[]
  (str "D:\\logs\\todo-" (c/to-long (t/now)) ".log"))

(defn- write [out msg]
  (.write out (str msg))
  (.flush out)
  out)

(def logger (agent (FileWriter. (log-file-name) true)))

(defn log [msg type]
  (send-off logger write (str "[" type "][" (t/now) "] - " msg "\r\n")))

(defn increment-counter[]
  (swap! login-via-vk-counter inc)
  (println @login-via-vk-counter)
  (if (== (mod @login-via-vk-counter 1000) 0)
    (log (str "counter = "@login-via-vk-counter) "log")))


